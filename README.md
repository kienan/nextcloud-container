# Nextcloud Container

## Running in a local development environment

### With traefik

This is done to because step closer to what a typical production deployment
looks like.

1. Create a web network, if it doesn't exist:

```
podman network exist web || podman network create web
```

2. Start traefik, if it isn't already running:

```
cd /path/to/traefik_project
podman-compose up -d
```

3. Copy the traefix router & service configuration

```
cd /path/to/nextcloud_project
cp examples/nc.toml /path/to/traefik_project/conf.d/
```

4. Start nextcloud

```
podman-compose up -d
```

5. Access at http://nc.localhost:8080

## Backing up the database

```
podman-compose exec nc-db mysqldump nextcloud | gzip -9 > nextcloud.sql.gz
```

## Production deployments

Rootless container for Nextcloud with the frontend being provided by Traefik,
which is running as a rootful container to bind to ports 80 and 443.

1. Create a user for the Nextcloud application (eg. nextcloud)
2. Enable linger for the user
3. Create the web network if it doesn't exist

```
sudo -u nextcloud podman network exists web || sudo -u nextcloud podman network create web
```

4. Deploy traefik configuration for the application, eg.

```
DEST_DIR=$(sudo podman volume inspect --format "{{.Mountpoint}}" traefik_config)
sudo cp /path/to/nextcloud/examples/nc.toml "${DEST_DIR}/"
sudo sed -i 's/nc\.localhost/HOSTNAME/g' "${DEST_DIR}/nc.toml"
sudo sed -i 's/http:\/\/nextcloud/http:\/\/localhost:9000/' "${DEST_DIR}/nc.toml"
```

5. Deploy a podman-compose configuration file, eg.

```
# ~/.config/containers/compose/projects/nextcloud
COMPOSE_PROJECT_DIR=/home/nextcloud/.../nc/
COMPOSE_FILE="container-compose.yml container-compose.prod.yml"
COMPOSE_PATH_SEPARATOR=:
COMPOSE_PROJECT_NAME=nc
```

6. Deploy a systemd service file for Nextcloud, eg.

```
# /etc/systemd/user/nextcloud.service
[Unit]
Description=Nextcloud Rootless Pod

[Service]
User=nextcloud
Type=simple
EnvironmentFile=%h/.config/containers/compose/projects/nc.env
ExecStartPre=-podman-compose up --no-start
ExecStartPre=/usr/bin/podman pod start pod_nc
ExecStart=podman-compose wait
ExecStop=/usr/bin/podman pod stop pod_nc
```

7. Reload systemd units

8. Deploy any necessary environment secret to the `.env` file, eg.
  * `MARIADB_PASSWORD`
  * `NC_TRUSTED_DOMAINS`

9. Start

```
sudo -u nextcloud systemd --user enable --now nextcloud.service
```
